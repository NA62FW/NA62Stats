Examples of statistical analyses performed with RooStats package
- The directory WSmaker contains the macros which create the RooWorkspace with the signal and background models  
- The directory ULmakers contains macros which take the RooWorkspace as input and perform the upper limit 

---------------------------------------------
---------------------------------------------
** Example 1:**
---------------------------------------------
0. If you are on lxplus, load ROOT, for example:
``` 
source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.20.00/x86_64-centos7-gcc48-opt/bin/thisroot.sh

```
1. WSmakers/wswrite_SmallStat.C
- Macro to prepare RooWorkspace to set an upper limit on a XS or BR or XS*BR 
   or a signal strength of a new process with the production of a new particle.
- Observable: a generic invariant mass of visible particles
- Signal shape is a gaussian 
- Background shape is a 2nd order polynomial
- Background yield modelled in the on/off approach (this is why the macro is named "SmallStat")

```
cd WSmakers
root -l -b -q  launchWS_SmallStat.C
```

it loops over 10 mass hypotheses, from 0.1 to 1 GeV/c^2 and run the macro wswrite_SmallStat.C to
create 10 root files with the model (and pdf files to visualize the models), each one for each mass hypotheses.
The dataset is created accordingly to two hypotheses: 
 (1) B-only and  (2) the presence of a signal with m=100 MeV/c^2 

----------------------------------------------

2. ULmakers/SetUL_freq.C
- Macro which performs a frequentist upper limit with ProfileLikelihood as test statistic
 
```
cd ../ULmakers
root -l -b -q launchUL_freq_SB_SmallStats.C 
```
It loops over 10 mass hypotheses and runs the macro SetUL_freq.C to perform the upper limit 
taking as input the root files created with WSmakers/wswrite_SmallStat.C and using as dataset 
the one with the presence of a signal


  
